from .State import State
from random import randint
import collections
import numpy
import math
import random
import datetime, time
import operator
import os

class Controller:

        Q = {}
        discount_factor = 0.9
        learning_rate = 0.5
        random_explore_coef = 0.0

	def __init__(self, load, state):
		self.state = state
		self.init_table_Q(load, state)


    # TODO: carrega a tabela Q de um arquivo (se load!=None, entao load ira conter o nome do arquivo a ser carregado),
    # ou, caso load==None, a funcao de inicializar uma tabela Q manualmente.
    # Dica: a tabela Q possui um valor para cada possivel par de estado e acao. Cada objeto do tipo State possui um id unico
    # (calculado por State.get_state_id), o qual pode ser usado para indexar a sua tabela Q, juntamente com o indice da acao.
    # Para criacao da tabela Q, pode ser importante saber o numero total de estados do sistema. Isso dependera de quantas features
    # voce utilizar e em quantos niveis ira discretiza-las (ver arquivo State.py para mais detalhes). O numero total de
    # estados do sistema pode ser obtido atraves do metodo State.get_n_states.
    # Uma lista completa com os estados propriamente ditos pode ser obtida atraves do metodo State.states_list.
	def init_table_Q(self,load, state):
		self.Q = collections.OrderedDict()
		if load == None:
			for currState in state.states_list():
				self.Q[state.get_state_id(currState)] = [0,0,0,0]
		else:
			f = open(load, 'r')
			contents = f.readlines()
			linenumber=0
			state_index = 0
			states_list = state.states_list()
			stateParams = []
			for line in contents:
				line = line.strip()
				if line == "":
					continue
				stateParams.append(float(line))
				linenumber = linenumber + 1
				if linenumber % 4 == 0:
					self.Q[state.get_state_id(states_list[state_index])] = stateParams
					state_index = state_index + 1
					stateParams = []
			print "Leitura de %d estados teve sucesso."
		print self.Q
		#exit()

	def save_table_Q(self, episode, state):
		#Escrever nesse arquivo que esta sendo criado na pasta params
		if episode > 0 and episode % 10 == 0:
			if not os.path.exists("./params"):
				os.makedirs("./params")
			output = open("./params/%s.txt" % datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S'), "w+")
                        for state_id, state_weights in self.Q.items():
                                for weight in state_weights:
                                        output.write(str(weight) + "\n")

	# TODO: funcao que calcula recompensa a cada passo
	# Recebe como o parametro a acao executada, o estado anterior e posterior a execucao dessa acao,
	# o numero de passos desde o inicio do episodio, e um booleano indicando se o episodio acabou apos a execucao da acao.
	# Caso o episodio tenha terminado, o ultimo parametro especifica como ele terminou (IA "won", IA "lost", "draw" ou "collision")
    # Todas essas informacoes podem ser usadas para determinar que recompensa voce quer dar para o agente nessa situacao
	def compute_reward(self, action, prev_state, curr_state, nsteps, isEpisodeOver, howEpisodeEnded):
        # Abaixo, um exemplo de funcao de recompensa super simples (mas provavelmente nao muito efetiva)
		estimatedReward = 0
		prev_features = prev_state.get_state()
		curr_features = curr_state.get_state()

		if action == 1:
			if prev_features[0] == 0:
				estimatedReward = estimatedReward + 5
			else:
				estimatedReward = estimatedReward - 5
		elif action == 3:
			if prev_features[0] == 1:
				estimatedReward = estimatedReward + 5
				if prev_features[2] >= 2:
					estimatedReward = estimatedReward + 5
					estimatedReward = estimatedReward + 5 + prev_features[2]
			else:
				estimatedReward = estimatedReward - 5
		elif action == 4:
			if prev_features[0] == 1:
				estimatedReward = estimatedReward + 5
				estimatedReward = estimatedReward + 5 - prev_features[2]
			else:
				estimatedReward = estimatedReward - 5
		return estimatedReward


	# TODO: Deve consultar a tabela Q e escolher uma acao de acordo com a politica de exploracao
	# Retorna 1 caso a acao desejada seja direita, 2 caso seja esquerda, 3 caso seja nula, e 4 caso seja atirar
	def take_action(self, state):
		values = self.Q[state.get_state_id(state.get_state())]
		if numpy.random.ranf() < 1 - self.random_explore_coef:
			return values.index(max(values)) + 1
		return numpy.random.choice([1,2,3,4])

	# TODO: Implementa a regra de atualziacao do Q-Learning.
	# Recebe como o parametro a acao executada, o estado anterior e posterior a execucao dessa acao,
	# a recompensa obtida e um booleano indicando se o episodio acabou apos a execucao da acao
	def updateQ(self, action, prev_state, curr_state, reward, isEpisodeOver):
		action = action - 1
		term = reward + self.discount_factor * numpy.max(self.Q[curr_state.get_state_id(curr_state.get_state())])
		result =  (1 - self.learning_rate) * self.Q[prev_state.get_state_id(prev_state.get_state())][action] + self.learning_rate * term
		self.Q[prev_state.get_state_id(prev_state.get_state())][action] = result
		return
